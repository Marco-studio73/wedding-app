import { IInvite } from './invite';

export class ITable {
  nom: string;
  invites: IInvite[];
  type: string;
  nbInvite: number;
}
