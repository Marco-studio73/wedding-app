import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss'],
})
export class CountdownComponent implements OnInit {

  private trialEndsAt;
  private diff: number;
  public days: number;
  public hours: number;
  public minutes: number;
  public seconds: number;

  constructor() { }

  ngOnInit() {
    this.trialEndsAt = '2021-03-13';
    interval(1000)
      .pipe(map((x) => {
        this.diff = Date.parse(this.trialEndsAt) - Date.parse(new Date().toString());
      })).subscribe((x) => {
        this.days = this.getDays(this.diff);
        this.hours = this.getHours(this.diff);
        this.minutes = this.getMinutes(this.diff);
        this.seconds = this.getSeconds(this.diff);
      });
  }

  getDays(t: number) {
    return Math.floor(t / (1000 * 60 * 60 * 24));

  }
  getHours(t: number) {
    return Math.floor((t / (1000 * 60 * 60)) % 24);
  }
  getMinutes(t: number) {
    return Math.floor((t / 1000 / 60) % 60);
  }
  getSeconds(t: number) {
    return Math.floor((t / 1000) % 60);
  }

}
