import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public cards = [
    {
      title: 'Lorem, ipsum dolor.',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, cupiditate.',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem, ipsum dolor.',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, cupiditate.',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem, ipsum dolor.',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, cupiditate.',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem, ipsum dolor.',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, cupiditate.',
      author: 'lorem ipsum'
    }
  ];

  public news = [
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },
    {
      title: 'Lorem ipsum dolor sit amet.',
      content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Suscipit illum quis voluptatum provident esse autem optio possimus tempore quia. Dolores distinctio excepturi delectus et soluta molestias facere id porro minus!',
      author: 'lorem ipsum'
    },

  ];

  constructor() { }

  ngOnInit(): void {
  }

}
