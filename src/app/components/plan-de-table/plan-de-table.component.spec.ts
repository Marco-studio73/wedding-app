import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanDeTableComponent } from './plan-de-table.component';

describe('PlanDeTableComponent', () => {
  let component: PlanDeTableComponent;
  let fixture: ComponentFixture<PlanDeTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanDeTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanDeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
