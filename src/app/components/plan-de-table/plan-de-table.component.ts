import { Component, OnInit } from '@angular/core';
import { IInvite } from 'src/app/models/invite';
import { ITable } from 'src/app/models/tables';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
  CdkDragEnd
} from '@angular/cdk/drag-drop';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CompleteTableComponent } from 'src/app/utils/snackbar/complete-table/complete-table.component';
import { InviteApi } from 'src/app/api/invite';
InviteApi;

@Component({
  selector: 'app-plan-de-table',
  templateUrl: './plan-de-table.component.html',
  styleUrls: ['./plan-de-table.component.scss']
})
export class PlanDeTableComponent implements OnInit {
  public query: string;
  public invites: IInvite[] = [];

  public tableList: ITable[] = [];
  public inviteList: IInvite[] = [];

  public addTable: FormGroup;

  constructor(
    private fb: FormBuilder,
    private _snackbar: MatSnackBar,
    private inviteApi: InviteApi
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getAllInvite();
  }

  getAllInvite() {
    this.inviteApi.getAllInvite().subscribe((listInvite: IInvite[]) => {
      this.invites = listInvite;
    });
  }

  public createForm() {
    this.addTable = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      type: [''],
      nbInvite: ['', [Validators.required, Validators.max(10)]]
    });
  }

  public createTable() {
    this.tableList.push({
      nom: this.addTable.value.name,
      type: this.addTable.value.type,
      invites: [],
      nbInvite: this.addTable.value.nbInvite
    });
    console.log(this.tableList);
    this.addTable.reset();
  }

  dragEnd(event: CdkDragEnd) {
    console.log('end drag', event);
  }

  drop(event: CdkDragDrop<IInvite[]>, table: ITable) {
    if (table && table.invites.length < table.nbInvite) {
      if (event.previousContainer === event.container) {
        moveItemInArray(
          event.container.data,
          event.previousIndex,
          event.currentIndex
        );
      } else {
        transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex
        );
      }
    } else if (!table) {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    if (table && table.invites.length === table.nbInvite) {
      this.displayCompleteMessage();
    }
  }

  displayCompleteMessage() {
    this._snackbar.openFromComponent(CompleteTableComponent, {
      data: { message: 'Table complete' },
      duration: 2000
    });
  }
}
