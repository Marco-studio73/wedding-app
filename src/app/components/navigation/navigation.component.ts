import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConnexionComponent } from '../connexion/connexion.component';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  public openConnectDialog() {
    this.dialog.open(ConnexionComponent);
  }

}
