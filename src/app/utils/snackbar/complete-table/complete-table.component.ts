import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-complete-table',
  templateUrl: './complete-table.component.html',
  styleUrls: ['./complete-table.component.scss']
})
export class CompleteTableComponent implements OnInit {
  public message: string;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {}

  ngOnInit(): void {
    this.message = this.data.message;
  }
}
