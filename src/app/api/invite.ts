import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { PlanDeTableService } from '../services/plan-de-table.service';
import { IInvite } from '../models/invite';

@Injectable()
export class InviteApi {
  constructor(private readonly planDeTableService: PlanDeTableService) {}

  getAllInvite(): Observable<IInvite[]> {
    return this.planDeTableService.getAllInvite().pipe(
      catchError(error => {
        throw error;
      }),
      map((listInvite: IInvite[]) => {
        return listInvite;
      })
    );
  }
}
