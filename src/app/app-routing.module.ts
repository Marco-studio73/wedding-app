import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { PlanDeTableComponent } from './components/plan-de-table/plan-de-table.component';
import { PrestaComponent } from './components/presta/presta.component';


const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'plan-de-table', component: PlanDeTableComponent },
  { path: 'prestas', component: PrestaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
