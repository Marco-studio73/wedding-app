import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IInvite } from '../models/invite';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class PlanDeTableService {
  private urlUser = environment.urls.invite;

  constructor(private _http: HttpService) {}

  getAllInvite(): Observable<IInvite[]> {
    return this._http.get(
      `${this.urlUser}/invite`,
      'GET_ALL_INVITE_START',
      'GET_ALL_INVITE_START'
    );
  }
}
