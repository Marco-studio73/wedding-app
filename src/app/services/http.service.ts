import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

@Injectable()
export class HttpService {
  constructor(private readonly http: HttpClient) {}

  public get(
    url: string,
    actionStart: string,
    actionEnd: string,
    token?: string,
    operation = 'operation'
  ): Observable<any> {
    const requestToken = uuid();
    const httpOptions = this.getHttpOptions(requestToken, token);
    return this.http.get<any>(url, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        this.handleError(err, url, actionEnd, requestToken, operation);
        throw err;
      })
    );
  }

  public post(
    url: string,
    post: any,
    actionStart: string,
    actionEnd: string,
    token?: string,
    operation = 'operation'
  ): Observable<any> {
    const requestToken = uuid();
    const httpOptions = this.getHttpOptions(requestToken, token);
    return this.http.post<any>(url, post, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        this.handleError(err, url, actionEnd, requestToken, operation);
        throw err;
      })
    );
  }

  public put(
    url: string,
    post: any,
    actionStart: string,
    actionEnd: string,
    token?: string,
    operation = 'operation'
  ): Observable<any> {
    const requestToken = uuid();
    const httpOptions = this.getHttpOptions(requestToken, token);
    return this.http.put<any>(url, post, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        this.handleError(err, url, actionEnd, requestToken, operation);
        throw err;
      })
    );
  }

  public patch(
    url: string,
    post: any,
    actionStart: string,
    actionEnd: string,
    token?: string,
    operation = 'operation'
  ): Observable<any> {
    const requestToken = uuid();
    const httpOptions = this.getHttpOptions(requestToken, token);
    return this.http.patch<any>(url, post, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        this.handleError(err, url, actionEnd, requestToken, operation);
        throw err;
      })
    );
  }

  public delete(
    url: string,
    actionStart: string,
    actionEnd: string,
    token?: string,
    operation = 'operation'
  ): Observable<any> {
    const requestToken = uuid();
    const httpOptions = this.getHttpOptions(requestToken, token);
    return this.http.delete<any>(url, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        this.handleError(err, url, actionEnd, requestToken, operation);
        throw err;
      })
    );
  }

  private getHttpOptions(
    requestToken: string,
    token?: string
  ): { headers: HttpHeaders } {
    let headers: HttpHeaders = new HttpHeaders({
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
      'X-Request-Token': requestToken
    });
    if (!_.isEmpty(token)) {
      headers = headers.append('x-session-token', `${token}`);
    }
    return { headers };
  }

  private handleError(
    error: HttpErrorResponse,
    url: string,
    action: string,
    requestToken: string,
    operation = 'operation'
  ) {
    const message = `${operation} failed: ${error.message}`;
  }
}
