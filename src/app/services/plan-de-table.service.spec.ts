import { TestBed } from '@angular/core/testing';

import { PlanDeTableService } from './plan-de-table.service';

describe('PlanDeTableService', () => {
  let service: PlanDeTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlanDeTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
