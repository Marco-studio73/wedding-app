// Modules
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

// Component
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainComponent } from './components/main/main.component';
import { PrestaComponent } from './components/presta/presta.component';
import { PlanDeTableComponent } from './components/plan-de-table/plan-de-table.component';
import { ConnexionComponent } from './components/connexion/connexion.component';
import { CountdownComponent } from './components/main/countdown/countdown.component';
import { CompleteTableComponent } from './utils/snackbar/complete-table/complete-table.component';

// Pipe
import { SearchPipe } from './pipe/search.pipe';

// Services
import { PlanDeTableService } from './services/plan-de-table.service';
import { InviteApi } from './api/invite';
import { HttpService } from './services/http.service';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FooterComponent,
    MainComponent,
    PrestaComponent,
    PlanDeTableComponent,
    ConnexionComponent,
    CountdownComponent,
    SearchPipe,
    CompleteTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [PlanDeTableService, HttpService, InviteApi],
  bootstrap: [AppComponent]
})
export class AppModule {}
