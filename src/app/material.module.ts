import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  imports: [
    MatDialogModule,
    MatTooltipModule,
    DragDropModule,
    MatSnackBarModule
  ],
  exports: [
    MatDialogModule,
    MatTooltipModule,
    DragDropModule,
    MatSnackBarModule
  ]
})
export class MaterialModule {}
